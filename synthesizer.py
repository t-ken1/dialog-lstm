import numpy as np
import torch
import pyworld
import pysptk
from nnmnkwii import paramgen
from nnmnkwii.io import hts
from nnmnkwii.frontend import merlin
from nnmnkwii.preprocessing import scale, minmax, meanvar, minmax_scale
from nnmnkwii.preprocessing import trim_zeros_frames
from nnmnkwii.postfilters import merlin_post_filter


class WaveSynthesizer(object):
    '''
    このクラスは肥大すぎるのでリファクタリング対象
    特徴量に関する処理が多いため、特徴量クラスを作る
    '''
    def __init__(self, acoustic_model, duration_model,
                 acoustic_stat, duration_stat,
                 feat_config, analysis_config, device='cpu'):
        self._field_initilization()

        self.model['acoustic'] = acoustic_model
        self.model['duration'] = duration_model

        self.stat['acoustic'] = acoustic_stat
        self.stat['duration'] = duration_stat

        self.config['feature'] = feat_config
        self.config['analysis'] = analysis_config

        self.device = device

    def _field_initilization(self):
        self.model = {}
        self.stat = {}
        self.config = {}

    def _get_acoustic_stat(self):
        return self.stat['acoustic']

    def _get_duration_stat(self):
        return self.stat['duration']

    def _get_feat_indices(self):
        feat_set = set(['mgc', 'lf0', 'vuv', 'bap'])
        indices = self.config['feature'].get_indices()
        if set(indices.keys()) != feat_set:
            raise ValueError("required feats are 'mgc', 'lf0', 'vuv', 'bap'.")
        return indices

    def _split_parameters(self, seq, indices):
        mgc = seq[:,:indices['lf0']]
        lf0 = seq[:,indices['lf0']:indices['vuv']]
        vuv = seq[:,indices['vuv']]
        bap = seq[:,indices['bap']:]

        return mgc, lf0, vuv, bap

    def _lf0_to_f0(self, lf0, vuv):
        f0 = lf0.copy()
        f0[vuv < 0.5] = 0
        f0[np.nonzero(f0)] = np.exp(f0[np.nonzero(f0)])
        
        return f0

    def generate_parameters(self, seq):
        T = seq.shape[0]
        config = self.config['analysis']
        
        feat_indices = self._get_feat_indices()
        mgc, lf0, vuv, bap = self._split_parameters(seq, feat_indices)
        var = self._get_acoustic_stat()['t_var']
        
        mgc_var = np.tile(var[:feat_indices['lf0']], (T, 1))
        lf0_var = np.tile(var[feat_indices['lf0']:feat_indices['vuv']], (T, 1))
        bap_var = np.tile(var[feat_indices['bap']:], (T, 1))

        mgc = paramgen.mlpg(mgc, mgc_var, config.window)
        lf0 = paramgen.mlpg(lf0, lf0_var, config.window)
        bap = paramgen.mlpg(bap, bap_var, config.window)
        
        return mgc, lf0, vuv, bap

    def generate_waveform(self, seq, do_postfilter=False):
        config = self.config['analysis']
        seq = trim_zeros_frames(seq)
        mgc, lf0, vuv, bap = self.generate_parameters(seq)

        if do_postfilter:
            mgc = merlin_post_filter(mgc, config.alpha)

        sp = pysptk.mc2sp(mgc, fftlen=config.fft_length, alpha=config.alpha)
        ap = pyworld.decode_aperiodicity(bap.astype(np.float64),
                                         config.sampling_rate,
                                         config.fft_length)
        f0 = self._lf0_to_f0(lf0, vuv)

        generated = pyworld.synthesize(f0.flatten().astype(np.float64),
                                       sp.astype(np.float64),
                                       ap.astype(np.float64),
                                       config.sampling_rate,
                                       config.frame_period)
        return generated

    def generate_duration(self, label_path, binary_dict, continuous_dict):
        hts_label = hts.load(label_path)
        features = merlin.linguistic_features(hts_label,
                                              binary_dict, continuous_dict,
                                              add_frame_features=False,
                                              subphone_features=None)
        features = features.astype(np.float32)

        model = self.model['duration']
        stat = self.stat['duration']
        
        model.to(self.device)
        model.eval()

        x = torch.tensor(features).float().to(self.device)
        xl = len(x)

        x = x.view(1, -1, x.size(-1))
        predicted = model(x, [xl]).cpu().data.numpy()
        predicted = np.round(predicted * stat['t_scale'] + stat['t_mean'])
        predicted[predicted <= 0] = 1
        hts_label.set_durations(predicted)

        return hts_label
        
    def generate_an_utterance(self, label_path, binary_dict, continuous_dict,
                              post_filter=True):

        conf = self.config['feature']
        stat = self.stat['acoustic']

        hts_label = self.generate_duration(label_path,
                                           binary_dict, continuous_dict)
        silence_indices = hts_label.silence_frame_indices()

        input_ = merlin.linguistic_features(
            hts_label, binary_dict, continuous_dict,
            add_frame_features=True, subphone_features=conf.subphone_feature
        )
        input_ = np.delete(input_, silence_indices, axis=0)
        input_ = minmax_scale(input_, stat['x_min'], stat['x_max'],
                              feature_range=(0.01, 0.99))

        model = self.model['acoustic'].to(self.device)
        model.eval()

        x = torch.tensor(input_).float().to(self.device)
        xl = len(x)

        x = x.view(1, -1, x.size(-1))
        predicted = model(x, [xl]).cpu().data.numpy()
        predicted = predicted.reshape(-1, predicted.shape[-1])

        predicted = predicted * stat['t_scale'] + stat['t_mean']

        return self.generate_waveform(predicted, do_postfilter=post_filter)
