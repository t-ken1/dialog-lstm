import numpy as np
import torch
import torch.utils.data
from sklearn.model_selection import train_test_split
from nnmnkwii.datasets import FileDataSource
from nnmnkwii.datasets import FileSourceDataset
from nnmnkwii.datasets import PaddedFileSourceDataset
from nnmnkwii.datasets import MemoryCacheDataset
from nnmnkwii.preprocessing import minmax, meanvar, minmax_scale, scale


class Dataset(object):
    def __init__(self, x_list, t_list, type_, feat_config, train_config):
        self._field_initialization()
        
        self.x_list = x_list
        self.t_list = t_list
        self.feat_type = type_
        self.config['feature'] = feat_config
        self.config['train'] = train_config

        for phase in ['train', 'test']:
            self.x[phase] = self._get_dataset(self.x_list, 'input', phase)
            self.t[phase] = self._get_dataset(self.t_list, 'target', phase)

        self.stat['x_min'], self.stat['x_max'] = self._get_x_stat()
        self.stat['t_mean'], self.stat['t_var'] = self._get_t_stat()
        self.stat['t_scale'] = np.sqrt(self.stat['t_var'])

    def _field_initialization(self):
        self.x = {}
        self.t = {}
        self.config = {}
        self.stat = {}

    def _get_dataset(self, list_, x_or_t, phase):
        return WrappedPaddedFileSourceDataset(
            BinaryFileSource(list_, self.feat_type, x_or_t, phase,
                             self.config['feature'], self.config['train'])
        )

    def _get_x_stat(self):
        return minmax(self.x['train'], self.x['train'].get_lengths())

    def _get_t_stat(self):
        return meanvar(self.t['train'], self.t['train'].get_lengths())

    def get_x(self, phase):
        return self.x[phase]

    def get_t(self, phase):
        return self.t[phase]
    
    def get_statistics(self):
        return self.stat
    
class PyTorchDataset(torch.utils.data.Dataset):
    def __init__(self, dataset, phase, cache_size=1000):
        if not isinstance(dataset, Dataset):
            raise TypeError('illegal dataset object.')

        self.x = MemoryCacheDataset(dataset.get_x(phase), cache_size)
        self.t = MemoryCacheDataset(dataset.get_t(phase), cache_size)
        self.lengths = dataset.get_x(phase).get_lengths()[:, None]

        self.x_min = dataset.get_statistics()['x_min']
        self.x_max = dataset.get_statistics()['x_max']
        self.t_mean = dataset.get_statistics()['t_mean']
        self.t_scale = dataset.get_statistics()['t_scale']

    def __getitem__(self, idx):
        x, t = self.x[idx], self.t[idx]
        x = minmax_scale(x, self.x_min, self.x_max, feature_range=(0.01, 0.99))
        t = scale(t, self.t_mean, self.t_scale)
        l = torch.from_numpy(self.lengths[idx])
        x, t = torch.from_numpy(x), torch.from_numpy(t)
        return x, t, l

    def __len__(self):
        return len(self.x)    

        
class BinaryFileSource(FileDataSource):
    '''
    nnmnkwii.datasets.PaddedFileSourceDataset を利用するために歪な実装に
    なっている。
    collect_files() と collect_feature() の引数を変更すると
    PaddedFileSourceDataset でこれらを利用しているメソッドすべてをオーバーライド
    しなければならず、変更が大規模になってしまうため。
    '''
    def __init__(self, paths, feat_kind, feat_phase, training_phase, 
                 feat_config, train_config):
        self.feature_kind = feat_kind
        self.config = {}
        self.config['feature'] = feat_config
        self.config['train'] = train_config

        self.is_x = True if feat_phase == 'input' else False
        self.is_train = True if training_phase == 'train' else False
        
        self.train_files, self.test_files = self._file_split(paths)
        
    def _file_split(self, paths):
        return train_test_split(paths,
                                test_size=self.config['train'].test_size,
                                random_state=self.config['train'].random_state)
    
    def collect_files(self):
        if self.is_train:
            return self.train_files
        else:
            return self.test_files

    def collect_features(self, path, dtype=np.float32):
        if self.is_x:
            dim = self.config['feature'].linguistic_dim[self.feature_kind]
        else:
            dim = self.config['feature'].parm_dim[self.feature_kind]
        return np.fromfile(path, dtype=dtype).reshape(-1, dim)


class WrappedPaddedFileSourceDataset(PaddedFileSourceDataset):
    '''
    PaddedFileSourceDataset の簡単なラッパー
    Paddedding する前の系列長系列を取得可能
    '''
    def __init__(self, source):
        self._set_lengths(source)
        super(self.__class__, self).__init__(
            source, self._get_max_length(source)
        )

    def _set_lengths(self, arg):
        data = FileSourceDataset(arg)
        self.lengths = np.array([len(x) for x in data], dtype=np.int)
        del data

    def _get_max_length(self, arg):
        return self.lengths.max()

    def get_lengths(self):
        return self.lengths
