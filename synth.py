import os
from os.path import join, basename, splitext
from glob import glob

import numpy as np
import torch
from torch.optim import Adam
from scipy.io import wavfile
from IPython.display import Audio
from nnmnkwii.io import hts

from configloader import ConfigLoader
from dataset import Dataset, PyTorchDataset
from model import SimpleLSTM, make_network
from trainer import Trainer, set_optimizer
from synthesizer import WaveSynthesizer


# ------------------------------------------------------------------
#  1. Configuration
# ------------------------------------------------------------------

data_root = './data/UUDB'
config_file = './config/trn.cnf'
config_loader = ConfigLoader(config_file)

train_config = config_loader.load('train')
feature_config = config_loader.load('feat')
network_config = config_loader.load('network')
analysis_config = config_loader.load('analysis')

synth_label_dir = './data/UUDB/test_label_phone_align'
synth_labels = glob(join(synth_label_dir, '*.lab'))

output_dir = './gen'

device = 'cuda' if torch.cuda.is_available() else 'cpu'

acoustic_input_list = glob(join(data_root, 'X_acoustic', '*.bin'))
duration_input_list = glob(join(data_root, 'X_duration', '*.bin'))
acoustic_target_list = glob(join(data_root, 'Y_acoustic', '*.bin'))
duration_target_list = glob(join(data_root, 'Y_duration', '*.bin'))

criterion = torch.nn.MSELoss()

question_file = './data/questions_lah2.hed'

# ------------------------------------------------------------------
#  2. Prepare Data
# ------------------------------------------------------------------
acoustic_dataset = Dataset(acoustic_input_list, acoustic_target_list,
                           'acoustic', feature_config, train_config)
duration_dataset = Dataset(duration_input_list, duration_target_list,
                           'duration', feature_config, train_config)

# ------------------------------------------------------------------
#  3. Make Model
# ------------------------------------------------------------------
acoustic_input_dim = feature_config.get_acoustic_linguistic_dim()
acoustic_output_dim = feature_config.get_acoustic_dim()

network_config.set_input_dim(acoustic_input_dim)
network_config.set_output_dim(acoustic_output_dim)
acoustic_model = make_network(SimpleLSTM, network_config)

duration_input_dim = feature_config.get_duration_linguistic_dim()
duration_output_dim = feature_config.get_duration_dim()

network_config.set_input_dim(duration_input_dim)
network_config.set_output_dim(duration_output_dim)
duration_model = make_network(SimpleLSTM, network_config)

print('--- Make Acoustic Model ---')
print(acoustic_model, '\n')

print('--- Make Duration Model ---')
print(duration_model, '\n')

# ------------------------------------------------------------------
#  4. Training
# ------------------------------------------------------------------
acoustic_optimizer = set_optimizer(Adam, acoustic_model, train_config)
duration_optimizer = set_optimizer(Adam, duration_model, train_config)

acoustic_trainer = Trainer(acoustic_model, acoustic_optimizer,
                           train_config, device=device)
duration_trainer = Trainer(duration_model, duration_optimizer,
                           train_config, device=device)

print('--- Training Acoustic Model ---')
acoustic_trainer.train(acoustic_dataset, criterion)

print('--- Training Duration Model ---')
duration_trainer.train(duration_dataset, criterion)

# ------------------------------------------------------------------
#  5. Generation Waveform
# ------------------------------------------------------------------
binary_dict, continuous_dict = hts.load_question_set(question_file)

acoustic_stat = acoustic_dataset.get_statistics()
duration_stat = duration_dataset.get_statistics()

synthesizer = WaveSynthesizer(
    acoustic_model, duration_model, acoustic_stat, duration_stat,
    feature_config, analysis_config, device=device
)

print('\n --- Synthesize ---')
for label_path in synth_labels:
    utt_id = splitext(basename(label_path))[0]

    print('synthesizing -- %s' % (utt_id))
    synthesized = synthesizer.generate_an_utterance(
        label_path, binary_dict, continuous_dict, post_filter=True
    )
    with open(join(output_dir, utt_id + '.wav'), 'wb') as f:
        f.write(Audio(synthesized, rate=analysis_config.sampling_rate).data)
