import torch
from torch import nn
from torch import optim
from configloader import NetworkConfig


class SimpleLSTM(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim,
                 num_layers=1, bidirectional=True):
        super(self.__class__, self).__init__()

        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        self.output_dim = output_dim
        self.num_direction = 2 if bidirectional else 1

        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers,
                            bidirectional=bidirectional, batch_first=True)
        self.hidden2out = nn.Linear(self.num_direction * self.hidden_dim,
                                    self.output_dim)

    def forward(self, sequence, lengths, hidden=None):
        sequence = nn.utils.rnn.pack_padded_sequence(sequence, lengths,
                                                     batch_first=True)
        output, (h, c) = self.lstm(sequence, hidden)
        output, _ = nn.utils.rnn.pad_packed_sequence(output,
                                                     batch_first=True)
        output = self.hidden2out(output)
        return output


def make_network(cls, config):
    if not isinstance(cls, type):
        raise TypeError('illegal network model.')
    if not isinstance(config, NetworkConfig):
        raise TypeError('illegal config object.')

    return cls(config.input_dim, config.hidden_dim, config.output_dim,
               num_layers=config.num_hidden_layers,
               bidirectional=config.bidirectional)
