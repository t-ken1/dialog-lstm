import configparser

import numpy as np
import pysptk
import pyworld


class ConfigSetter(object):
    def __init__(self, config_parser, kind):
        self.config_parser = config_parser
        self.kind = kind
        
    def _to(self, str_value, function):
        if not isinstance(str_value, str):
            raise TypeError("first argument is not 'str'.")
        if not callable(function):
            raise TypeError("second argument is not 'callable'.")
        return function(str_value)
        
    def to_int(self, arg):
        return self._to(arg, int)

    def to_float(self, arg):
        return self._to(arg, float)

    def to_bool(self, arg):
        if not isinstance(arg, str):
            raise TypeError("required argument is 'str'.")
        return (arg == 'True')

    def get_value(self, arg):
        return self.config_parser.get(self.kind, arg)


class FeatureConfig(ConfigSetter):
    def __init__(self, config_parser, kind):
        super(self.__class__, self).__init__(config_parser, kind)
        self._field_initilization()

        self.feat_dim['mgc'] = self.to_int(self.get_value('mgc_dim'))
        self.feat_dim['lf0'] = self.to_int(self.get_value('lf0_dim'))
        self.feat_dim['bap'] = self.to_int(self.get_value('bap_dim'))
        self.feat_dim['vuv'] = self.to_int(self.get_value('vuv_dim'))

        self.linguistic_dim['acoustic'] = self.to_int(
            self.get_value('acoustic_linguistic_dim')
        )
        self.linguistic_dim['duration'] = self.to_int(
            self.get_value('duration_linguistic_dim')
        )

        self.parm_dim['acoustic'] = self._get_acoustic_dim()
        self.parm_dim['duration'] = 1
        self.subphone_feature = self.get_value('subphone_feat')

        self.feat_index['mgc'] = 0
        self.feat_index['lf0'] = self.feat_index['mgc'] + self.feat_dim['mgc']
        self.feat_index['vuv'] = self.feat_index['lf0'] + self.feat_dim['lf0']
        self.feat_index['bap'] = self.feat_index['vuv'] + self.feat_dim['vuv']

    def _field_initilization(self):
        self.feat_dim = {}
        self.parm_dim = {}
        self.linguistic_dim = {}
        self.feat_index = {}

    def _get_acoustic_dim(self):
        result = 0
        for feat in ['mgc', 'lf0', 'bap', 'vuv']:
            result += self.feat_dim[feat]
        return result

    def get_acoustic_dim(self):
        return self.parm_dim['acoustic']

    def get_duration_dim(self):
        return self.parm_dim['duration']

    def get_acoustic_linguistic_dim(self):
        return self.linguistic_dim['acoustic']

    def get_duration_linguistic_dim(self):
        return self.linguistic_dim['duration']

    def get_indices(self):
        return self.feat_index


class AnalysisConfig(ConfigSetter):
    def __init__(self, config_parser, kind):
        super(self.__class__, self).__init__(config_parser, kind)
        self.sampling_rate = self.to_int(self.get_value('sampling_rate'))
        self.frame_period = self.to_int(self.get_value('frame_period'))
        self.has_delta = self.to_bool(self.get_value('has_delta'))

        if self.has_delta:
            self.window = [
                (0, 0, np.array([1.0])),
                (1, 1, np.array([-0.5, 0.0, 0.5])),
                (1, 1, np.array([1.0, -2.0, 1.0]))
            ]
        else:
            self.window = [(0, 0, np.array([1.0]))]

        self.fft_length = pyworld.get_cheaptrick_fft_size(
            self.sampling_rate
        )
        self.alpha = pysptk.util.mcepalpha(self.sampling_rate)
        self.hop_length = int(
            self.sampling_rate * 0.001 * self.frame_period
        ) # require [Hz] -> [kHz]


class TrainConfig(ConfigSetter):
    def __init__(self, config_parser, kind):
        super(self.__class__, self).__init__(config_parser, kind)
        self.test_size = self.to_float(self.get_value('test_size'))
        self.random_state = self.to_int(self.get_value('random_state'))

        self.num_epoch = self.to_int(self.get_value('num_epoch'))
        self.batch_size = self.to_int(self.get_value('batch_size'))
        self.weight_decay = self.to_float(self.get_value('weight_decay'))
        self.learning_rate = self.to_float(self.get_value('learning_rate'))

        self.num_workers = self.to_int(self.get_value('num_workers'))
        self.use_pin_memory = self.to_bool(self.get_value('use_pin_memory'))
        self.cache_size = self.to_int(self.get_value('cache_size'))


class NetworkConfig(ConfigSetter):
    def __init__(self, config_parser, kind):
        super(self.__class__, self).__init__(config_parser, kind)
        try:
            self.input_dim = self.to_int(self.get_value('input_dim'))
        except:
            self.input_dim = FeatureConfig(
                config_parser, 'feat'
            ).linguistic_dim['acoustic']

        self.hidden_dim = self.to_int(self.get_value('hidden_dim'))
        self.num_hidden_layers = self.to_int(
            self.get_value('num_hidden_layers')
        )

        try:
            self.output_dim = self.to_int(self.get_value('output_dim'))
        except:
            self.output_dim = FeatureConfig(
                config_parser, 'feat'
            ).parm_dim['acoustic']

        self.bidirectional = self.to_bool(self.get_value('bidirectional'))

    def set_input_dim(self, arg):
        self.input_dim = arg

    def set_output_dim(self, arg):
        self.output_dim = arg


class ConfigLoader(object):
    def __init__(self, file_path):
        self.config_parser = configparser.ConfigParser()
        self.config_parser.read(file_path)

    def load(self, kind):
        if kind == 'analysis':
            return AnalysisConfig(self.config_parser, kind)
        elif kind == 'feat':
            return FeatureConfig(self.config_parser, kind)
        elif kind == 'train':
            return TrainConfig(self.config_parser, kind)
        elif kind == 'network':
            return NetworkConfig(self.config_parser, kind)
        else:
            raise ValueError('illegal config type.')
