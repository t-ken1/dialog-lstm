import torch
import torch.nn as nn
from nnmnkwii.datasets import MemoryCacheDataset

from dataset import PyTorchDataset
from configloader import TrainConfig


class Trainer(object):
    def __init__(self, model, optimizer, config, device='cpu'):
        self.model = model.to(device)
        self.optimizer = optimizer
        self.config = config
        self.device = device

    def make_loader(self, dataset):
        cache_size = self.config.cache_size
        train_dataset = PyTorchDataset(dataset, 'train', cache_size)
        test_dataset = PyTorchDataset(dataset, 'test', cache_size)

        train_loader = torch.utils.data.DataLoader(
            train_dataset, batch_size=self.config.batch_size,
            num_workers=self.config.num_workers, shuffle=True
        )
        test_loader = torch.utils.data.DataLoader(
            test_dataset, batch_size=self.config.batch_size,
            num_workers=self.config.num_workers, shuffle=True
        )
        return train_loader, test_loader
        

    def train(self, dataset, criterion_):
        train_loader, test_loader = self.make_loader(dataset)
        criterion = criterion_

        self.model.train()

        for epoch in range(1, self.config.num_epoch+1):
            for phase in ['train', 'test']:
                running_loss = 0.0
                loader = train_loader if phase == 'train' else test_loader

                for x, t, lens in loader:
                    sorted_lens, indices = torch.sort(lens.view(-1),
                                                      dim=0, descending=True)
                    x, t = x[indices], t[indices]
                    t = t[:, :sorted_lens.long().numpy()[0]]

                    x = x.to(self.device)
                    t = t.to(self.device)
                    sorted_lens = sorted_lens.to(self.device)

                    self.optimizer.zero_grad()
                    t_hat = self.model(x, sorted_lens)
                    loss = criterion(t_hat, t)

                    if phase == 'train':
                        loss.backward()
                        self.optimizer.step()
                    running_loss += loss.item()

                if phase == 'train' and (epoch % 20) == 0:
                    print("epoch:%d, loss:%.3f" % (epoch, running_loss))


def set_optimizer(cls, model, config):
    if not isinstance(cls, type):
        raise TypeError('illegal optimizer.')
    if not isinstance(config, TrainConfig):
        raise TypeError('illegal config object.')

    return cls(model.parameters(), lr=config.learning_rate,
               weight_decay=config.weight_decay)
